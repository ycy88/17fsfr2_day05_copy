const express = require('express');
const fs = require('fs');

// modify your images folder 
var folder = __dirname + '/public/images';

// set params
var picArray = fs.readdirSync(folder);
var picsNumber = picArray.length;

// open express
var app = express();

// routes

app.get('/randomimg', function(req, res){
  sendImage(res);
});
// invoke the res object of the app instance
// pass the res object as a parameter into the sendImage function 
// execute the res objects with the params passed to it by sendImage 


  //callback sendImage
  function sendImage(res) {
    var randomImage = picArray[Math.floor((Math.random()*picsNumber))];
    res.status(200);
    res.type('jpg');
    res.sendFile(randomImage, { root: folder });
  }

// function generateImg(res) {
//   var imgSrc = picArray[Math.floor((Math.random()*picsNumber))];
//   res.status(200);
//   res.type('text/html');
//   res.send("<img src = '/images/" + imgSrc + "' height = '400px'>");
//   // careful when you interpolate the html fragment - need to double escape commas
// }

// app.get('/randomimgpath', function(req, res){
//   generateImg(res);
// });

app.use(express.static(__dirname + '/public'));


// open the port 
var port = parseInt(process.argv[2]) || 3000;

app.listen(port, function(){
  console.log('Server is now listening on %s', port);
});